<?php
class ImaggaClient {
  const HOST = 'api.imagga.com';
  const VERSION = 'v1';
  const SCHEME = 'https';

  private $api_key;
  private $api_secret;

  function ImaggaClient($api_key, $api_secret) {
    $this->api_key = $api_key;
    $this->api_secret = $api_secret;
  }

  private function send_request($type, $method = 'GET', $params = array()) {
    if ($method == 'GET') {
      $request_uri = $this->build_request_uri($type, $params);
      $response = drupal_http_request($request_uri, array('method' => $method));
    }
    else if ($method == 'POST') {
      $boundary = 'A0sFSD';
      $headers = array(
        'Content-Type' => 'multipart/form-data; boundary=' . $boundary,
        'Accept' => 'application/json',
      );

      $request_uri = $this->build_request_uri($type);
      $response = drupal_http_request($request_uri, array('headers' => $headers, 'method' => $method, 'data' => $this->multipart_encode($boundary, $params)));
    }

    if ($response->code < 400) {
      return json_decode($response->data);
    }
    else {
      throw new ImaggaException($response->data, $response->code);
    }
  }

  private function build_request_uri($type, $params = array()) {
    $query = http_build_query($params, NULL, '&');
    $query = preg_replace('/%5B(?:[0-9]|[1-9][0-9]+)%5D=/', '=', $query);

    $request_uri = self::SCHEME . '://' . $this->api_key . ':' . $this->api_secret . '@' . self::HOST . '/' . self::VERSION . '/' . $type;
    $request_uri .= ($query)? '?' . $query : '';
    return $request_uri;
  }

  private function add_file_params(Array $file_urls = NULL, Array $content_ids = NULL) {
    $params = array(
      'url' => array(),
      'content' => array(),
    );

    if ($file_urls) {
      foreach($file_urls as $url) {
        if ($url) {
          $params['url'][] = $url;
        }
        unset($url);
      }
    }

    if ($content_ids) {
      foreach($content_ids as $id) {
        if ($id) {
          $params['content'][] = $id;
        }
        unset($id);
      }
    }

    return $params;
  }

  function upload_content($file) {

    $params = array('image' => realpath(".") . '/' . $file->source);
    return $this->send_request('content', 'POST', $params);
  }

  function multipart_encode($boundary, $params){
    $output = "";
    foreach ($params as $key => $value){
      $output .= "--$boundary\r\n";
      if ($key == 'image'){
        $output .= $this->multipart_enc_file($value);
      } else $output .= $this->multipart_enc_text ($key, $value);
    }
    $output .="--$boundary--";
    return $output;
  }

  function multipart_enc_text($name, $value){
    return "Content-Disposition: form-data; name=" . $name . "\r\n\r\n$value\r\n";
  }

  function multipart_enc_file($path){
    if (substr($path, 0, 1) == "@") $path = substr($path, 1);
    $filename = basename($path);
    $mimetype = "application/octet-stream";
    $data = 'Content-Disposition: form-data; name=image; filename=' . $filename . "\r\n";
    $data .= "Content-Transfer-Encoding: binary\r\n";
    $data .= "Content-Type: $mimetype\r\n\r\n";
    $data .= file_get_contents($path) . "\r\n";
    return $data;
  }

  function get_colors($file_url = NULL, $content_id = NULL, $extract_overall_colors = '1', $extract_object_colors = '1') {
    return $this->get_colors_multitple(array($file_url), array($content_id), $extract_overall_colors, $extract_object_colors);
  }

  function get_colors_multitple(Array $file_urls = NULL, Array $content_ids = NULL, $extract_overall_colors = '1', $extract_object_colors = '1') {

    $params = $this->add_file_params($file_urls, $content_ids);
    $params['extract_overall_colors'] = $extract_overall_colors;
    $params['extract_object_colors'] = $extract_object_colors;

    return $this->send_request('colors', 'GET', $params);
  }

  function get_croppings($file_url = NULL, $content_id = NULL, $width = NULL, $height = NULL, $no_scaling = NULL) {
    return $this->get_croppings_multitple(array($file_url), array($content_id), $width, $height, $no_scaling);
  }

  function get_croppings_multitple(Array $file_urls = NULL, Array $content_ids = NULL, $width = NULL, $height = NULL, $no_scaling = NULL) {
    $params = $this->add_file_params($file_urls, $content_ids);

    if ($width && $height) {
      $params['resolution'] = $width . 'x' .  $height;
    }

    if ($no_scaling) {
      $params['no_scaling'] = $no_scaling;
    }

    return $this->send_request('croppings', 'GET', $params);
  }

  function get_categorizations($categorizer_id, $file_url = NULL, $content_id = NULL, Array $languages = NULL) {
    return $this->get_categorizations_multiple($categorizer_id, array($file_url), array($content_id), $languages);
  }

  function get_categorizations_multiple($categorizer_id, Array $file_urls = NULL, Array $content_ids = NULL, Array $languages = NULL) {
    $params = $this->add_file_params($file_url, $content_ids);

    if ($languages) {
      $params['language'] = $languages;
    }

    return $this->send_request('categorizations/' . $categorizer_id, 'GET', $params);
  }

  function get_categorizers() {
    return $this->send_request('categorizers', 'GET', $params);
  }

  function get_tagging($file_url = NULL, $content_id = NULL, Array $languages = NULL) {
    return $this->get_tagging_multiple(array($file_url), array($content_id), $languages);
  }

  function get_tagging_multiple(Array $file_urls = NULL, Array $content_ids = NULL, Array $languages = NULL) {
    $params = $this->add_file_params($file_urls, $content_ids);

    if ($languages) {
      $params['language'] = $languages;
    }

    return $this->send_request('tagging', 'GET', $params);
  }
}

class ImaggaCroppingsAdaptor {
  private $imagga;
  private $image;
  private $content_id;

  function ImaggaCroppingsAdaptor(ImaggaClient $imagga, $image) {
    $this->image = $image;
    $this->imagga = $imagga;
  }

  function upload_content() {
    $uploaded = $this->imagga->upload_content($this->image);
    $this->content_id = $uploaded->uploaded[0]->id;
  }

  function get_crop_dimensions($width, $height, $no_scaling = NULL) {
    if (!$this->content_id) {
      $this->upload_content();
    }
    $croppings = $this->imagga->get_croppings(NULL, $this->content_id, $width, $height, $no_scaling);
    return $croppings->results[0]->croppings[0];
  }

}

class ImaggaException extends Exception {}
