<?php

function imagga_settings_form($form, &$form_state) {
  $form['imagga_api_key'] = array(
    '#title' => t('API Key'),
    '#type' => 'textfield',
    '#default_value' => variable_get('imagga_api_key', ''),
  );

  $form['imagga_api_secret'] = array(
    '#title' => t('API Secret'),
    '#type' => 'textfield',
    '#default_value' => variable_get('imagga_api_secret', ''),
  );

  return system_settings_form($form);
}
